import os
import json
import time
import datetime
import youtube_dl
from pydub import AudioSegment


class BadTimeFormat(Exception):
    pass


class Part:
    def __init__(self, name: str, start_time: str, end_time: str):
        self._name = name
        self._start_time = self._string_to_delta(start_time)
        self._end_time = self._string_to_delta(end_time)

    def _delta_to_string(self, delta: datetime.timedelta) -> str:
        return datetime.datetime.strftime(datetime.datetime.strptime(str(delta), "%H:%M:%S"), "%H:%M:%S")

    def _string_to_delta(self, time_str) -> datetime.timedelta:
        time_obj = None

        if time_str.count(':') == 2:
            time_obj = time.strptime(time_str, '%H:%M:%S')
        elif time_str.count(':') == 1:
            time_obj = time.strptime(time_str, '%M:%S')
        else:
            raise BadTimeFormat

        return datetime.timedelta(hours=time_obj.tm_hour,
                                  minutes=time_obj.tm_min,
                                  seconds=time_obj.tm_sec)

    @property
    def name(self) -> str:
        return self._name

    @property
    def start_time_ms(self) -> int:
        return int(self._start_time.total_seconds()) * 1000

    @property
    def end_time_ms(self) -> int:
        return int(self._end_time.total_seconds()) * 1000

    @property
    def as_string(self) -> str:
        return f'name: {self._name}\r\n' \
               f'start: {self._delta_to_string(self._start_time)}\r\n' \
               f'end: {self._delta_to_string(self._end_time)}\r\n' \
               f'length: {self._delta_to_string(self._end_time - self._start_time)}'


def get_config(path: str) -> dict:
    with open(path, 'r') as f:
        return json.load(f)

def download_mp3_from_yt(url: str, file_path: str):
    ydl_opts = {'format': 'bestaudio/best',
                'outtmpl': f'{file_path}',
                'postprocessors': [{
                    'key': 'FFmpegExtractAudio',
                    'preferredcodec': 'mp3',
                    'preferredquality': '192'}]}

    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        ydl.download([url])


def get_parts(movements: dict) -> list:
    for k, v in movements.items():
        yield Part(name=k, start_time=v['start'], end_time=v['end'])

def main():
    config = get_config('config.json')

    file_path = os.path.join(config['input_directory'], config['file_name'])

    if 'yt_url' in config:
        download_mp3_from_yt(url=config['yt_url'], file_path=file_path)

    audio = AudioSegment.from_file(file_path)

    for part in get_parts(config['parts']):
        mp3_part = audio[part.start_time_ms:part.end_time_ms]
        export_path = os.path.join(config['output_directory'],
                                   f'{os.path.splitext(config["file_name"])[0]} - {part.name}.mp3')
        mp3_part.export(export_path, format='mp3')

        print(f'Exported part to path: {export_path}')
        print(part.as_string)
        print('---------------')
        print()


if __name__ == '__main__':
    main()
